import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";
import { ApiModel, ApiModelProperty } from "swagger-express-ts";


@Entity()
@ApiModel( {
    description : "Version description" ,
    name : "pupils"
} )
export class Pupils {
    
    @ApiModelProperty( {
        description : "ID учащегося" ,
        required : true,
        example: 123456789
    } )
    @PrimaryGeneratedColumn()
    id: number;

    @ApiModelProperty( {
        description : "Имя" ,
        required : true,
        example: 'firstname'
    } )
    @Column()
    firstName: string;

    @ApiModelProperty( {
        description : "Фамилия" ,
        required : true,
        example: 'lastname'
    } )
    @Column()
    lastName: string;

    @ApiModelProperty( {
        description : "Отчество" ,
        required : false,
        example: 'lastname'
    } )
    @Column()
    middleName: string;

    @ApiModelProperty( {
        description : "Дата рождения" ,
        required : false,
        example: '1970-01-01 00:00:00'
    } )
    @Column()
    dateBirth: Date;

    @ApiModelProperty( {
        description : "Успеваемость" ,
        required : false,
        example: 'неуд/уд/хор/отл'
    } )
    @Column()
    evaluation: string;

}
