import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Pupils} from "../entity/Pupils";
import { ApiModel, ApiModelProperty, ApiPath, ApiOperationGet, ApiOperationPost, ApiOperationPatch, SwaggerDefinitionConstant, ApiOperationDelete } from "swagger-express-ts";

const evaluations: Array<object> = [{name: "неуд"},{name: "уд"},{name: "хор"},{name: "отл"}];


@ApiModel( {
	description : "Version description" ,
	name : "evaluations"
} )

class Evaluations {
	@ApiModelProperty( {
		description : "Успеваимость" ,
		required : true,
		example: 'test'
	} )
	name: string;
}

@ApiPath({
    path: "/pupels",
    name: "Pupils",
    security: { basicAuth: [] }
})

export class PupilController {

    private pupilRepository = getRepository(Pupils);
    
    @ApiOperationGet({
		path: '',
		description: 'Получение всех учащихся с оценками',
		summary: 'Получение всех учащихся с оценками',
		parameters: {
			query: {
			},
		},
		responses: {
			200: {
				description: 'OK',
				type: SwaggerDefinitionConstant.Response.Type.ARRAY,
				model: 'pupils',
			}
		},
	})

    async all(request: Request, response: Response, next: NextFunction) {
        return this.pupilRepository.find();
    }

    @ApiOperationGet({
		path: '/:id',
		description: 'Получение одного учащегося',
		summary: 'Получение одного учащегося',
		parameters: {
			query: {
			},
		},
		responses: {
			200: {
				description: 'OK',
				type: SwaggerDefinitionConstant.Response.Type.OBJECT,
				model: 'pupils',
			},
            400: {
				description: 'Bad Request',
				type: SwaggerDefinitionConstant.Response.Type.OBJECT,
				model: '',
			},
		},
	})

    async one(request: Request, response: Response, next: NextFunction) {
        return this.pupilRepository.findOne(request.params.id);
    }

    @ApiOperationPost({
		path: '',
		description: 'Сохранение учащегося',
		summary: 'Сохранение учащегося',
		parameters: {
			query: {
        firstName: {
					description: 'Имя',
					required: true,
					type: SwaggerDefinitionConstant.Response.Type.STRING,
				},
        lastName: {
					description: 'Фамилия',
					required: true,
					type: SwaggerDefinitionConstant.Response.Type.STRING,
				},
        middleName: {
					description: 'Отчество',
					required: true,
					type: SwaggerDefinitionConstant.Response.Type.STRING,
				},
        dateBirth: {
					description: 'День рождения',
					required: true,
					type: SwaggerDefinitionConstant.Response.Type.STRING,
				},
				evaluation: {
					description: 'Успеваемость',
					required: false,
					type: SwaggerDefinitionConstant.Response.Type.STRING,
				}

			},
		},
		responses: {
			200: {
				description: 'OK',
				type: SwaggerDefinitionConstant.Response.Type.OBJECT,
				model: 'pupils',
			},
            400: {
				description: 'Bad Request',
				type: SwaggerDefinitionConstant.Response.Type.OBJECT,
				model: '',
			},
		},
	})

    async save(request: Request, response: Response, next: NextFunction) {
        return this.pupilRepository.save(request.body);
    }

    @ApiOperationPatch({
		path: '/:id',
		description: 'Редактирование одного учащегося',
		summary: 'Редактирование одного учащегося',
		parameters: {
			query: {
                firstName: {
					description: 'Имя',
					required: false,
					type: SwaggerDefinitionConstant.Response.Type.STRING,
				},
                lastName: {
					description: 'Фамилия',
					required: false,
					type: SwaggerDefinitionConstant.Response.Type.STRING,
				},
                middleName: {
					description: 'Отчество',
					required: false,
					type: SwaggerDefinitionConstant.Response.Type.STRING,
				},
                dateBirth: {
					description: 'День рождения',
					required: false,
					type: SwaggerDefinitionConstant.Response.Type.STRING,
				},
                evaluation: {
					description: 'Успеваемость',
					required: false,
					type: SwaggerDefinitionConstant.Response.Type.STRING,
				}

			},
		},
		responses: {
			200: {
				description: 'OK',
				type: SwaggerDefinitionConstant.Response.Type.OBJECT,
				model: 'pupils',
			},
            400: {
				description: 'Bad Request',
				type: SwaggerDefinitionConstant.Response.Type.OBJECT,
				model: '',
			},
		},
	})
    async edit(request: Request, response: Response, next: NextFunction) {
        let pupilToSelect = await this.pupilRepository.findOne(request.params.id);
        let pupil = await this.pupilRepository.merge(pupilToSelect, request.body)
        return this.pupilRepository.save(pupil);
    }

    @ApiOperationDelete({
			path: '/:id',
			description: 'Удаление учащегося',
			summary: 'Удаление учащегося',
			parameters: {
				query: {
				},
			},
			responses: {
				200: {
					description: 'OK',
					type: SwaggerDefinitionConstant.Response.Type.OBJECT,
					model: 'pupils',
				},
							400: {
					description: 'Bad Request',
					type: SwaggerDefinitionConstant.Response.Type.OBJECT,
					model: '',
				},
			},
		})

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.pupilRepository.findOne(request.params.id);
        return await this.pupilRepository.remove(userToRemove);
    }

		@ApiOperationGet({
			path: '/evaluation',
			description: 'Получение списка успеваимости',
			summary: 'Получение списка успеваимости',
			parameters: {
				query: {
				},
			},
			responses: {
				200: {
					description: 'OK',
					type: SwaggerDefinitionConstant.Response.Type.ARRAY,
					model: 'evaluations',
				},
							400: {
					description: 'Bad Request',
					type: SwaggerDefinitionConstant.Response.Type.ARRAY,
					model: '',
				},
			},
		})

    async evaluation(request: Request, response: Response, next: NextFunction) {
        return evaluations;
    }

}