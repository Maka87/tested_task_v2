import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import {Routes} from "./routes";
import {Pupils} from "./entity/Pupils";
import * as swagger from "swagger-express-ts";
import { SwaggerDefinitionConstant } from "swagger-express-ts";

createConnection().then(async connection => {

    // create express app
    const app = express();
    app.use( '/api-docs/swagger' , express.static( 'swagger' ) );
    app.use( '/api-docs/swagger/assets' , express.static( 'node_modules/swagger-ui-dist' ) )
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use( swagger.express(
      {
        definition : {
          info : {
              title : "API doc \"Tested task\"" ,
              version : "1.0"
          } ,
          externalDocs : {
              url : "http://localhost:3001/"
          }
          // Models can be defined here
        }
      }
    ));
    // register express routes from defined application routes
    Routes.forEach(route => {
        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    // setup express app here
    // ...

    // start express server
    app.listen(3001);

    // insert new users for test
    // await connection.manager.save(connection.manager.create(Pupils, {
    //     firstName: "Максим",
    //     lastName: "Курганов",
    //     middleName: "Вячеславович",
    //     dateBirth: new Date('1987-04-19 21:00:00'),
    //     evaluation: "неуд"
    // }));
    // await connection.manager.save(connection.manager.create(Pupils, {
    //     firstName: "Юлия",
    //     lastName: "Курганова",
    //     middleName: "Фёдоровна",
    //     dateBirth: new Date('1987-12-09  09:00:00'),
    //     evaluation: "хор"
    // }));

    console.log("Express server has started on port 3001. Open http://localhost:3001/pupils to see results");

}).catch(error => console.log(error));
