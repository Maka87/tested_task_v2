import { PupilController } from "./controller/PupilController";

export const Routes = [{
    method: "get",
    route: "/pupils",
    controller: PupilController,
    action: "all"
}, {
    method: "get",
    route: "/pupils/:id",
    controller: PupilController,
    action: "one"
}, {
    method: "post",
    route: "/pupils",
    controller: PupilController,
    action: "save"
}, {
    method: "path",
    route: "/pupils/:id",
    controller: PupilController,
    action: "edit"
}, {
    method: "delete",
    route: "/pupils/:id",
    controller: PupilController,
    action: "remove"
}, {
    method: "get",
    route: "/pupils/evaluation",
    controller: PupilController,
    action: "evaluation"
}];
